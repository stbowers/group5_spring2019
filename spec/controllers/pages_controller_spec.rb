require 'rails_helper'

RSpec.describe PagesController, type: :controller do

  context 'When not signed in' do
      describe "GET #home" do
      it "returns http success" do
      get :home
      expect(response).to have_http_status(:success)
    end
  end
end
  context "When signed in" do
    sign_me_in
      describe "Redirect to task_path" do
      it "Redirects the current user" do
        get :home
        expect(response).to redirect_to(tasks_path)
    end
  end  
  end
end
