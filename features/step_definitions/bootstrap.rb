Then (/^I should see the Task Header$/) do
    # The user should see the Headers
	expect(page).to have_content("Productivity Game")
	expect(page).to have_content("Current Tasks")
end