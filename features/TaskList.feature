#feature/TaskList.feature
Feature: Task List
    As a user of the app
    I want to be able to click on the task list link
    So that I can see my full list of past and previous tasks
    
Scenario: User can see a list of tasks
    Given I am logged in
    And I am on the tasks page
    Then I should see a list of current tasks
    