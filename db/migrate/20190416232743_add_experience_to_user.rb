class AddExperienceToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.integer :experience,        default: 0
    end
  end
end