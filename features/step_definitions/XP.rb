#test task experience reward happy-path
Then(/^I should see my xp is now "(.*)"$/) do |xp|
    expect(page).to have_content(xp)
end