# feature/add_tasks.feature
Feature: Add tasks
    As a user of the app
    I want to be able to add tasks
    So that I can track tasks I need to complete

Scenario: User can see add task view
    Given I am logged in
    And I am on the tasks page
    When I click add task
    Then I should see the add task view

Scenario: User can add task
    Given I am logged in
    And I am on the new_task page
    When I fill in a task with name New_task
    Then I should see the following tasks: New_task

Scenario: Add task fails
    Given I am logged in
    And I am on the new_task page
    When I fill in a task with name New_task
    Then I should not see the following tasks: New_task
