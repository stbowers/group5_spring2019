require 'rails_helper.rb'

FactoryBot.define do
    factory :User do
        email {'sample@gmail.com'}
        password {'somepassword'}
    end
end

#RSpec.describe User, type: :controller
describe User do
    #sign_in

    context "when testing the levels" do    
        it "The currentlevel should return level" do
            example_user = User.create
            expect(example_user.level).to eq(1)
        end
    end

    context "when testing the rank" do    
        it "The current level should not be nil" do
            example_user = User.create
            expect(example_user.level).to eq(1)
        end
    end

    context "ranks" do
        it "rank should be equal to Newb" do
            for level in (1..3) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Newb"
            end
        end
        it "rank should be equal to Peasant" do
            for level in (4..6) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Peasant"
            end
        end
        it "rank should be equal to Militia" do
            for level in (7..10) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Militia"
            end
        end
        it "rank should be equal to Foot Soldier" do
            for level in (11..13) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Foot Soldier"
            end
        end
        it "rank should be equal to Squire" do
            for level in (14..18) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Squire"
            end
        end
        it "rank should be equal to Knight" do
            for level in (19..23) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "Knight"
            end
        end
        it "rank should be equal to General" do
            for level in (24..50) do
                example_user = User.create
                example_user.stub(:level => level)
                expect(example_user.rank).to eq "General"
            end
        end
        it "rank should be equal to Empty Value" do
            for level in (0..0) do
                example_user = User.create
                example_user.stub(:level => 0)
                expect(example_user.rank).to eq "Empty Value"
            end
        end
    end
    context "Profile picture" do
        it  "profile picture should be visible" do
            example_user = User.create
            expect(example_user.profile_picture).to eq "/profile_pic.png"
        end
    end
    context "Level definitions check" do
        it "minimum experience should be equal to 16 times the previous level squared" do
            example_user = User.create
            expect(example_user.minExpForLevel).to eq (((example_user.level - 1)*(example_user.level - 1)) * 16)
        end
        it "maximum experience should be equal to 16 times the current level squared" do
            example_user = User.create
            expect(example_user.maxExpForLevel).to eq (((example_user.level)*(example_user.level))*16)
        end
        it "experience bar should be a percentage of the amount of experience needed between current level and next level" do
            example_user = User.create
            expect(example_user.levelprogress).to eq (((example_user.experience - example_user.minExpForLevel).to_f/(example_user.maxExpForLevel - example_user.minExpForLevel).to_f) * 100).floor
        end
    end
            
end
