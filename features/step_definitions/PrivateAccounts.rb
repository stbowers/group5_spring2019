#Test Private Accounts

Given(/I am trying to create a new account/) do
    # Go to home page
    visit('/')
    # Click sign up
    click_link("Sign up")
end

Given(/^an account exists with email "(.*)" and password "(.*)"$/) do |email, password|
    # Make a new user
    User.create(:email => email, :password => password)
end

When(/I set up my new account/) do
    # Fill in email
    fill_in("user_email", :with => "email@email.com")

    # Fill in password
    fill_in("user_password", :with => "password")
    fill_in("user_password_confirmation", :with => "password")

    # Click sign up
    click_button("Sign up")
end

When(/^I log in to "(.*)" with password "(.*)"$/) do |email, password|
    # Click sign in
    click_link("Sign in")

    # Fill in email
    fill_in("user_email", :with => email)

    # Fill in password
    fill_in("user_password", :with => password)

    # Click sign in
    click_button("Log in")
end

When(/^I try to sign up with email "(.*)" and password "(.*)"$/) do |email, password|
    # Go to home page
    visit('/')
    # Click sign up
    click_link("Sign up")

    # Fill in email
    fill_in("user_email", :with => email)

    # Fill in password
    fill_in("user_password", :with => password)
    fill_in("user_password_confirmation", :with => password)

    # Click sign up
    click_button("Sign up")
end

Then(/I should see my account be created/) do
    # I should be redirected to the tasks page
    expect(page).to have_current_path("/tasks")
    page.has_text?("email@email.com")
end

Then(/^I should get a message saying email is already taken$/) do
    #Expect error
    expect(page).to have_content("Email has already been taken")
end

Then(/^I should be logged in as "(.*)"$/) do |email|
    expect(page).to have_content(email)
end