#test diffculty selection happy-path

When(/^I select difficulty "(.*)"$/) do |difficulty|
    select(difficulty, {from: "task[difficulty]"})
end

Then(/^The task difficulty should be "(.*)"$/) do |difficulty|
    expect(page).to have_content(difficulty)
end
