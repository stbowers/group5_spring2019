When(/^I log in as "(.*)"$/) do |user|
    # Create test user account
    User.create(:email => user, :password => "password")

    # Log in
    # Go to the sign in page
    click_link("Sign in")

    # Fill in email
    fill_in("user_email", :with => user)

    # Fill in password
    fill_in("user_password", :with => "password")

    # Click sign in
    click_button("Log in")
end

When(/^I create a task named "(.+)"$/) do |task|
    # Click on the add task link
    click_link("Add new task")

    # Fill in details
    fill_in("task_name", :with => task)

    # Add task
    click_button("Add")

    # Refresh page to get rid of flash message before checking for task
    visit "/"
end

When (/^I log out$/) do
    # Sign out
    click_link("Sign Out")
end

Then (/^I should (not )?see "(.*)" in the task list$/) do |no_task, task|
    if !no_task then
        # expect tasks to be in the list
        expect(page).to have_content(task)
    else
        expect(page).to_not have_content(task)
    end
end
