# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

tasks = [
    {:name => 'Take out trash', :difficulty => 'Easy', :notes => 'Livingroom stinks', :progress => 'incomplete'},
	{:name => 'Iteration 0-3', :difficulty => 'Epic', :notes => 'Make main view', :progress => 'complete'},
	{:name => 'Present Iter 0', :difficulty => 'Hard', :notes => 'Show progress on app', :progress => 'in_progress'},
	{:name => 'Mow lawn', :difficulty => 'Hard', :notes => 'Yard stinks', :progress => 'incomplete'},
	{:name => 'Clean cat litter', :difficulty => 'Medium', :notes => 'Cat box stinks', :progress => 'incomplete'},
	{:name => 'Walk dog', :difficulty => 'Medium', :notes => 'Dog needs to walk', :progress => 'complete'},
]

tasks.each do |task|
  Task.create!(task)
end
