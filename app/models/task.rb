# Task active record
class Task < ActiveRecord::Base
    def self.all_progress
        return ['Not started', 'In progress', 'Complete']
    end

    def points
        case self.difficulty
        when "Newb"
            return 25
        when "Medium"
            return 50
        when "Hard"
            return 75
        when "Death Wish"
            return 100
        else
            return 0
        end
    end
end