# Test Task Status
Then("the task should show progress {string}") do |string|
    expect(page).to have_content(string)
end