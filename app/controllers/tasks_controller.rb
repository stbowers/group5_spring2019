# Tasks controller
class TasksController < ApplicationController

  def task_params
    params.require(:task).permit(:name, :difficulty, :notes, :progress)
  end

  def index
    if params[:update_status]
      @task = Task.find(params[:id])
      @current_status = @task[:progress]
      if @current_status == "Not started"
        @current_status = "In progress"
      elsif @current_status == "In progress"
        @current_status = "Complete"
        current_user[:experience] += @task.points
        current_user.save()
        # add to "user.exp"
        # jump to ____.rb to check for level-up
      else
        @current_status = "Complete"
      end

      @task.progress = @current_status
      @task.save()
    end

    @sort_by =
      if params.key?(:sort_by)
        params[:sort_by]
      elsif session.key?(:sort_by)
        reload = true
        session[:sort_by]
      else
        ''
      end
    @tasks = Task.where({user_id: current_user.email}).order(@sort_by)
    @all_progress = Task.all_progress
    
    
        @ratings =
      if params.key?(:progress)
        params[:progress]
      elsif session.key?(:progress)
        reload = true
        session[:progress]
      else
        Hash[@all_progress.collect {|progress| [progress, 1]}]
      end
  end

  def create
    task_fields = task_params
    task_fields[:progress] = "Not started"
    task_fields[:user_id] = current_user.email
    #task_fields[:difficulty] = "Easy"
    @task = Task.create!(task_fields)
    flash[:notice] = "#{@task.name} was successfully created."
    #suggestion = :label
    
    #    if suggestion.first == "D"
    #       suggestion = "Do Laundry"
    #    end
        
    redirect_to tasks_path
  end

end
