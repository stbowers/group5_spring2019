require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  context "When signed in" do
    sign_me_in

    describe "creating a new task" do
      it "validates parameters" do
        controller.params = ActionController::Parameters.new({:task => {
          :name => "Test",
          :difficulty => "Hard",
          :notes => "Notes",
          :progress => "Not started",
          :unexpected_value => "Unexpected"
          }})
        expect(controller.task_params).not_to be_nil()
        expect(controller.task_params).to include(:name)
        expect(controller.task_params).to include(:difficulty)
        expect(controller.task_params).to include(:notes)
        expect(controller.task_params).to include(:progress)
        expect(controller.task_params).not_to include(:unexpected_value)
      end

      it "redirects to tasks_path" do
        controller.params = ActionController::Parameters.new({:task => {
          :name => "Test",
          :difficulty => "Hard",
          :notes => "Notes",
          :progress => "Not started",
          :unexpected_value => "Unexpected"
          }})

        post :create, controller.params

        expect(response).to redirect_to(tasks_path)
      end
    end

    describe "loading index" do
      it "shows" do
        id = {:id => "id"}
        get :index
        expect(assigns(@task).nil?).to eq(false)
      end
      it "assigns @tasks" do
        get :index
        expect(assigns(@task).nil?).to eq(false)
      end
      it "renders the index template" do
        get :index
        expect(response).to render_template("index")
      end
      it "assigns sort by" do
        get :index
        expect(assigns(@sort_by).nil?).to eq(false)
      end
      it "assigns ratings" do
        get :index
        expect(assigns(@ratings).nil?).to eq(false)
      end
    end

    describe "updating task status" do
      not_started_task = Task.create(:name => "Name", :notes => "Notes", :progress => "Not started", :difficulty => "Newb")
      in_progress_task = Task.create(:name => "Name", :notes => "Notes", :progress => "In progress", :difficulty => "Newb")
      complete_task = Task.create(:name => "Name", :notes => "Notes", :progress => "Complete", :difficulty => "Newb")

      it "changes the task status from not started to in progress" do
        get :index, id: not_started_task.id, update_status: true
        updated_task = Task.where({:id => not_started_task.id})[0]
        expect(updated_task.progress).to eq("In progress")
      end

      it "changes the task status from in progress to not started" do
        get :index, id: in_progress_task.id, update_status: true
        updated_task = Task.where({:id => in_progress_task.id})[0]
        expect(updated_task.progress).to eq("Complete")
      end

      it "does not change complete tasks" do
        get :index, id: complete_task.id, update_status: true
        updated_task = Task.where({:id => complete_task.id})[0]
        expect(updated_task.progress).to eq("Complete")
      end
    end
  end
end