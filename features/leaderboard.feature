# feature/leaderboard.feature
Feature: leaderboard
    As a competitive user of the app
    I want to be able to display my weekly achievements/points/number of completed tasks
    So that I can compare my week to my friends to see who had a more productive week.

    Background:
        Given these users exist
        | email          | password | experience |
        | test@test.com  | password | 15         |
        | test2@test.com | password | 10         |

    
Scenario: Leaderboard ranks users
    Given I am on the leaderboard page
    Then I should see "test@test.com" before "test2@test.com" on the leaderboard

Scenario: User earns XP and rank changes on leaderboard
    Given I am on the leaderboard page
    And "test2@test.com" earned 20 EXP
    When the page is reloaded
    Then I should see "test2@test.com" before "test@test.com" on the leaderboard