class AddUserIdToTasks < ActiveRecord::Migration
  def change
    change_table :tasks do |t|
      t.string :user_id, default: "default@test.com"
    end
  end
end
