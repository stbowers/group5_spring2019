class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string   :name
      t.string   :difficulty
      t.text     :notes
      t.string   :progress
    end
  end
end
