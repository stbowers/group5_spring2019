Given(/^I am level (.*)$/) do |level|
    user = User.where({:email => "test@test.com"})[0]
    needed_xp = (16 * ((level.to_i - 1) * (level.to_i - 1))).ceil
    user.experience = needed_xp
    user.save()
end

Then(/^my level should be greater than (.*)$/) do |level|
    user = User.where({:email => "test@test.com"})[0]
    expect(user.level).to be > level.to_i
end

Then(/^my rank should (not )?be "(.*)"$/) do |is_not, rank|
    user = User.where({:email => "test@test.com"})[0]
    if is_not
        expect(user.rank).not_to eq(rank)
    else
        expect(user.rank).to eq(rank)
    end
end