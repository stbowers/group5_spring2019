# feature/tasks_per_user.feature
Feature: Tasks Per User
    As a user of the app
    I want tasks to be tied to my account
    So that I can see my own tasks and not other users tasks

Scenario: User can see their own tasks
    Given I am on the home page
    When I log in as "test1@test.com"
    And I create a task named "Test 1"
    Then I should see "Test 1" in the task list

Scenario: User cannot see other users tasks
    Given I am on the home page
    When I log in as "test1@test.com"
    And I create a task named "Test 2"
    And I log out
    And I log in as "test2@test.com"
    Then I should not see "Test 2" in the task list