require 'rails_helper'
RSpec.describe Task, type: :model do

  describe "Task" do
    it "Lists all valid progress values" do
        expect(Task.all_progress).to eq(['Not started', 'In progress', 'Complete'])
    end

    it "Gives points for difficulties" do
        example_task = Task.create(:name => "Example", :difficulty => "Newb")
        expect(example_task.points).not_to be_nil()
    end
    
    it "Gives 25 points for 'Newb' difficulty" do
        example_task = Task.create(:name => "Example", :difficulty => "Newb")
        expect(example_task.points).to eq(25)
    end

    it "Gives 50 points for 'Medium' difficulty" do
        example_task = Task.create(:name => "Example", :difficulty => "Medium")
        expect(example_task.points).to eq(50)
    end

    it "Gives 75 points for 'Hard' difficulty" do
        example_task = Task.create(:name => "Example", :difficulty => "Hard")
        expect(example_task.points).to eq(75)
    end

    it "Gives 100 points for 'Death Wish' difficulty" do
        example_task = Task.create(:name => "Example", :difficulty => "Death Wish")
        expect(example_task.points).to eq(100)
    end

    it "Gives 0 points for unknown difficulty" do
        example_task = Task.create(:name => "Example", :difficulty => "N/A")
        expect(example_task.points).to eq(0)
    end
  end

end
