# User active record
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


    def rank
        currentlevel = level
        if (currentlevel >= 1 && currentlevel < 4)
            return "Newb"
        elsif (currentlevel >= 4 && currentlevel < 7)
            return "Peasant"
        elsif (currentlevel >= 7 && currentlevel < 11)
             return "Militia"
        elsif (currentlevel >= 11 && currentlevel < 14)
            return "Foot Soldier"
        elsif (currentlevel >= 14 && currentlevel < 19)
            return "Squire"
        elsif (currentlevel >= 19 && currentlevel < 24)
            return "Knight"
        elsif (currentlevel >= 24)
            return "General"
        else
            return "Empty Value";
        end
    end

    def profile_picture
        return '/profile_pic.png'
    end
    def level  
        return (0.25*Math.sqrt(self.experience)).floor + 1
    end
    
    def minExpForLevel
        currentlevel = level - 1
        return 16*(currentlevel*currentlevel)
    end
    
    def maxExpForLevel
        currentlevel = level
        return 16*(currentlevel*currentlevel)
    end
    
    def levelprogress
        currentexperience = self.experience
        minexperience = minExpForLevel
        maxexperience = maxExpForLevel
        return (((currentexperience - minexperience).to_f/(maxexperience - minexperience).to_f) * 100).floor
    end
  
end

