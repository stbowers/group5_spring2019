# feature/XP.feature
Feature: Experience Points (Built off of the Points feature)
  As a regular user of the app
  I want to earn points for every task I complete
  So that I can see my progress and have an incentive to complete tasks.

Scenario: User completes a task
  Given I am logged in
  And I am on the tasks page
  And the task "Test Task 1" exists
  When I click "Not started"
  And I click "In progress"
  Then I should see my xp is now "25"
  
Scenario: User completes "easy" task
  Given I am logged in
  And I am on the tasks page
  And the task "Test Task 1" exists with difficulty "Newb"
  When I click "Not started"
  And I click "In progress"
  Then I should see my xp is now "25"

Scenario: User completes "medium" task
  Given I am logged in
  And I am on the tasks page
  And the task "Test Task 1" exists with difficulty "Medium"
  When I click "Not started"
  And I click "In progress"
  Then I should see my xp is now "50"

Scenario: User completes "hard" task
  Given I am logged in
  And I am on the tasks page
  And the task "Test Task 1" exists with difficulty "Hard"
  When I click "Not started"
  And I click "In progress"
  Then I should see my xp is now "75"

Scenario: User completes "Epic/Death Wish" task
  Given I am logged in
  And I am on the tasks page
  And the task "Test Task 1" exists with difficulty "Death Wish"
  When I click "Not started"
  And I click "In progress"
  Then I should see my xp is now "100"