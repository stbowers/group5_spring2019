# feature/bootstrap.feature
Feature: Bootstrap
    Everyone wants to use a professional-looking, visually appealing app
    As a regular user of the Productivity Game application
    I want to see the Task Header on the main page
    So that I can use a visually appealing and professional-looking app.

Scenario: I see the categories
    Given I am logged in
    And I am on the tasks page
    Then I should see the Task Header