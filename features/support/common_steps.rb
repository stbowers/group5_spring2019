Given(/^Pending/) do
    skip_this_scenario
end

Given(/^I am logged in( as .*)?$/) do |as_user|
    # Go home
    visit "/"

    # Create a user account
    email = if as_user != nil then as_user[3..-1] else "test@test.com" end
    User.create(:email => email, :password => "password")

    # Click sign in
    click_link("Sign in")

    # Fill in email
    fill_in("user_email", :with => email)

    # Fill in password
    fill_in("user_password", :with => "password")

    # Click sign in
    click_button("Log in")
end

Given(/^I am on the (.*) page$/) do |path|
    case path
    # Special cases
    when /^home$/
        visit("/")
    # Get the path from rails (i.e. the new_task page => new_task_path)
    else
        rails_path = self.send((path+"_path").to_sym)
        visit(rails_path)
    end
end

Given(/^the task "(.*)" exists$/) do |task|
    click_link("Add new task")
    fill_in("task_name", :with => name)
    click_button("Add")
end

Given(/^the task "(.*)" exists with difficulty "(.*)"$/) do |task, difficulty|
    click_link("Add new task")
    fill_in("task_name", :with => name)
    select(difficulty, {from: "task[difficulty]"})
    click_button("Add")
end

Given(/^these users exist$/) do |users|
    users.hashes.each do |user|
        User.create(:email => user["email"], :password => user["password"], :experience => user["experience"])
    end
end
Given(/^the following tasks exist$/) do |tasks|
    visit tasks_path
    tasks.hashes.each do |task|
        #Task.create(:name => task["name"], :notes => task["notes"], :difficulty => task["difficulty"], :progress => task["progress"])
        click_link("Add new task")
        fill_in("task_name", :with => task["name"])
        fill_in("task_notes", :with => task["notes"])
        select(task["difficulty"], {from: "task[difficulty]"})
        click_button("Add")
        if task["progress"] == "In progress"
            click_link("Not started")
        end
    end
end

When(/^I click "(.*)"$/) do |link|
    click_link(link)
end

When(/^the page is reloaded$/) do
    visit [ current_path, page.driver.request.env['QUERY_STRING'] ].reject(&:blank?).join('?')
end