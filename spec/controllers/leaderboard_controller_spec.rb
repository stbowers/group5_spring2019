require 'rails_helper'

RSpec.describe LeaderboardController, type: :controller do
    context "When signed in" do
        sign_me_in
        describe "list of users" do
            it "is assigned" do
                get :index
                expect(assigns(@list)).not_to be_nil()
            end
        end  
    end
end
