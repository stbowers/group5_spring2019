# Controller for the leaderboard
class LeaderboardController < ApplicationController
  
  def index
    @list = User.all.order(:experience).reverse_order[0..4]
  end
  
end
