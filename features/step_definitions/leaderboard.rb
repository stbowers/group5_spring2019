# Cucumber tests for the leader board

Given (/^"(.*)" earned (.*) EXP$/) do |user, xp|
    user = User.where({:email => user})[0]
    user.experience += xp.to_i
    user.save()
end

Then(/^I should see "(.*)" before "(.*)" on the leaderboard$/) do |user1, user2|
    regexp = /.*#{Regexp.escape(user1)}.*#{Regexp.escape(user2)}/
    expect(page.text).to match(regexp)
end