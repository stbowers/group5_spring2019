require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
    context "when testing current or guest user" do
        it "makes sure the method exists" do
          expect(ApplicationController.respond_to?(:current_or_guest_user)).to be(false)
        end
      end
      context "when testing guest user" do
        it "makes sure the method exists" do
          expect(ApplicationController.respond_to?(:guest_user)).to be(false)
        end
      end
    context "when testing the guest user" do
        it "gets cached guest user" do
          expect(assigns(@cached_guest_user).nil?).to eq(false)
        end
      end
      context "when testing logging in" do
        it "makes sure the method exists" do
          expect(ApplicationController.respond_to?(:logging_in)).to be(false)
        end
      end
      context "when testing create guest user" do
        it "makes sure the method exists" do
          expect(ApplicationController.respond_to?(:create_guest_user)).to be(false)
        end
      end

end