# Test adding a task

When(/^I click add task$/) do
    # Click on the add task link
    click_link("Add new task")
end

When(/^I fill in a task with name (.+)$/) do |name|
    # Fill in name, notes, difficulty, etc
    fill_in("task_name", :with => name)
    click_button("Add")
end

Then (/^I should see the add task view$/) do
    # expect to be on add task page
    expect(page).to have_current_path(new_task_path)
end

Then (/^I should be brought back to the task list page$/) do
    # expect to be on task list page
    assert page.current_path == task_path
end

Then(/^I should (not )?see the following tasks: (.+)$/) do |no_task, tasks|
    # expect tasks to be in the list
    list = tasks.split(/\s*,\s*/).each do |task|
        expect(page).to have_content(task)
    end
end
