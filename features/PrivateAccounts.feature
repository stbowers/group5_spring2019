# feature/PrivateAccounts.feature
Feature: Private Accounts
  As a regular user of the app
  I want to be able to create my own personal account
  So that I can see my progress, tasks, and other things related to my account

Scenario: User can create an account
  Given I am trying to create a new account
  When I set up my new account
  Then I should see my account be created

Scenario: User can log into their account
  Given an account exists with email "email@example.com" and password "password"
  And I am on the home page
  When I log in to "email@example.com" with password "password"
  Then I should be logged in as "email@example.com"

Scenario: User cannot sign up with a taken email
  Given an account exists with email "email@example.com" and password "password"
  When I try to sign up with email "email@example.com" and password "password123"
  Then I should get a message saying email is already taken
  
