#feature/TaskStatus.feature
Feature: Task Status
    As a gamer who uses the app to increase my productivity
    I want to be able to click on the status of a task
    So that I can see it change from not_started to in_progress, from in_progress to complete, or from complete to in_progress.
    
Scenario: User clicks the task status when status is Not started
    Given I am logged in
    And I am on the tasks page
    And the task "Test Task 1" exists
    When I click "Not started"
    Then the task should show progress "In progress"

Scenario: User clicks the task status when status is In progress
    Given I am logged in
    And I am on the tasks page
    And the task "Test Task 1" exists
    When I click "Not started"
    And I click "In progress"
    Then the task should show progress "Complete"
    
Scenario: User clicks the task status when status is complete
    Given I am logged in
    And I am on the tasks page
    And the task "Test Task 1" exists
    When I click "Not started"
    And I click "In progress"
    And I click "Complete"
    Then the task should show progress "Complete"