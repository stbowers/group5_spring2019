# feature/levels.feature
Feature: Levels
    As a regular user of the app
    I want to level up as I earn points
    So that I can compare my progress to other users
Background:
    Given these users exist
    | email          | password | experience |
    | test@test.com  | password | 140        |
    And I am logged in as test@test.com
    And the following tasks exist
    | name      | notes | difficulty | progress    |
    | Test Task | note  | Newb       | In progress |

Scenario: User levels up from completing a task
    Given I am on the tasks page
    When I click "In progress"
    Then my level should be greater than 1
    And my rank should not be "Newb"
    
Scenario: Levels 1-3 = Newb
    Given I am level 1
    Then my rank should be "Newb"

Scenario: Levels 4-7 = Peasant
    Given I am level 4
    Then my rank should be "Peasant"

Scenario: Levels 7-11 = Militia
    Given I am level 7
    Then my rank should be "Militia"

Scenario: Levels 11-14 = Foot Soldier
    Given I am level 11
    Then my rank should be "Foot Soldier"

Scenario: Levels 14-19 = Squire
    Given I am level 14
    Then my rank should be "Squire"

Scenario: Levels 19-24 = Knight
    Given I am level 19
    Then my rank should be "Knight"

Scenario: Levels 24+ = General
    Given I am level 24
    Then my rank should be "General"