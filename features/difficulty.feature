Feature: Chooseable Difficulty
    As an honorable user of the app
    I want to be able to set some tasks at different difficulties than others
    So that I am not getting the same rewards for easy tasks as I am for difficult ones
    
Scenario: User selecting the difficulty "Newb" for the task
    Given I am logged in
    And I am on the new_task page
    When I select difficulty "Newb"
    And I fill in a task with name "Test"
    Then The task difficulty should be "Newb"
    
Scenario: User Selecting the difficulty "Medium" for the task
    Given I am logged in
    And I am on the new_task page
    When I select difficulty "Medium"
    And I fill in a task with name "Test"
    Then The task difficulty should be "Medium"

Scenario: User selecting the difficulty "Hard" for the task
    Given I am logged in
    And I am on the new_task page
    When I select difficulty "Hard"
    And I fill in a task with name "Test"
    Then The task difficulty should be "Hard"
    
Scenario: User Selecting the difficulty "Death Wish" for the task
    Given I am logged in
    And I am on the new_task page
    When I select difficulty "Death Wish"
    And I fill in a task with name "Test"
    Then The task difficulty should be "Death Wish"
